FROM million12/nginx-php

WORKDIR /var/www/apps/api.lunch.skyrocket.is/
COPY ./ /var/www/apps/api.lunch.skyrocket.is/
RUN composer install

# Small amount of custom config
COPY ./docker-config/nginx.conf /etc/nginx/hosts.d/default.conf
COPY ./docker-config/php-fpm-www-env.conf /data/conf/
