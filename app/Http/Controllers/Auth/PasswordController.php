<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\ResetsPasswords;
use Illuminate\Http\JsonResponse;

class PasswordController extends Controller
{

    use ResetsPasswords;

    /**
     * Get the response for after the reset link has been successfully sent.
     *
     * @param string $response
     *
     * @return \Symfony\Component\HttpFoundation\Response
     */
    protected function getSendResetLinkEmailSuccessResponse($response)
    {
        return response()->json([
            'success' => 'Email Sent',
        ], 200);
    }

    /**
     * Get the response for after the reset link could not be sent.
     *
     * @param string $response
     *
     * @return \Symfony\Component\HttpFoundation\Response
     */
    protected function getSendResetLinkEmailFailureResponse($response)
    {
        return new JsonResponse(['email' => trans($response)], 422);
    }

    /**
     * Get the response for after a successful password reset.
     *
     * @param string $response
     *
     * @return \Symfony\Component\HttpFoundation\Response
     */
    protected function getResetSuccessResponse($response)
    {
        return response()->json([
          'success' => 'Password Reset',
        ], 200);
    }

    /**
     * Get the response for after a failing password reset.
     *
     * @param Request $request
     * @param string  $response
     *
     * @return \Symfony\Component\HttpFoundation\Response
     */
    protected function getResetFailureResponse(Request $request, $response)
    {
        return new JsonResponse(['email' => trans($response)], 422);
    }
}
