<?php

use Illuminate\Routing\Router;

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/
Route::group(['prefix' => 'api/v1', 'groups' => 'api'], function(Router $router){

    Route::group(['prefix' => 'auth', 'namespace' => 'Auth'], function(Router $router) {
      $router->post('login', 'AuthController@login');
      $router->post('register', 'AuthController@register');
      $router->post('password', 'PasswordController@sendResetLinkEmail');
      $router->post('password/reset', 'PasswordController@reset');
    });

    $router->get('review', 'ReviewController@index');
    $router->get('review/{review}', 'ReviewController@show');

    $router->get('location', 'LocationController@index');
    $router->get('location/{location}', 'LocationController@show');

    Route::group(['middleware' => 'jwt.auth'], function(Router $router) {
        $router->post('review', 'ReviewController@store');
        $router->post('location', 'LocationController@store');
    });

});
