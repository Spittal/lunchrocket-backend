<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Location extends Model
{

    /**
     * Fields that can be mass assigned.
     *
     * @var array
     */
    protected $fillable = [
        'name',
        'address1',
        'address2',
        'city',
        'province',
        'country',
        'postal_code',
        'lat',
        'lng'
    ];

    public function reviews() {
        return $this->hasMany(Review::class);
    }
}
