<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Rating extends Model
{
    protected $fillable = [
        'rating',
        'user_id'
    ];
    
    public function review() {
        return $this->hasOne(Review::class);
    }

    public function user() {
        return $this->hasOne(User::class);
    }
}
