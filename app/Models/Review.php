<?php

namespace App\Models;

use App\Models\Rating;
use Illuminate\Database\Eloquent\Model;

class Review extends Model
{

    /**
     * Items that can be mass assigned.
     *
     * @var array
     */
    protected $fillable = [
        'title',
        'body'
    ];

    public function location()
    {
        return $this->belongsTo(Location::class);
    }

    public function user()
    {
        return $this->belongsTo(User::class);
    }

    public function ratings()
    {
        return $this->hasMany(Rating::class);
    }

    public function primaryRating()
    {
        return $this->hasOne(Rating::class)->where('user_id', $this->user_id);
    }
}