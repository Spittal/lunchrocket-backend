<?php

/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| Here you may define all of your model factories. Model factories give
| you a convenient way to create models for testing and seeding your
| database. Just tell the factory how a default model should look.
|
*/

use App\Models\Location;
use App\Models\Review;
use App\Models\User;

$factory->define(App\Models\User::class, function (Faker\Generator $faker) {
    return [
        'name' => $faker->name,
        'email' => $faker->safeEmail,
        'password' => bcrypt(str_random(10)),
        'remember_token' => str_random(10),
    ];
});

$factory->define(App\Models\Location::class, function (Faker\Generator $faker) {
    return [
        'address1' => $faker->streetName,
        'address2' => $faker->streetAddress,
        'city' => $faker->city,
        'province' => $faker->state,
        'country' => $faker->country,
        'postal_code' => $faker->postcode,
    ];
});

$factory->define(App\Models\Review::class, function (Faker\Generator $faker) {
    return [
        'title' => $faker->streetName,
        'body' => $faker->streetAddress,
        'location_id' => factory(Location::class)->create()->id,
        'user_id' => factory(User::class)->create()->id,
    ];
});

$factory->define(App\Models\Rating::class, function (Faker\Generator $faker) {
    $review = factory(Review::class)->create();
    return [
        'rating' => 5,
        'review_id' => $review->id,
        'user_id' => $review->user->id,
    ];
});
